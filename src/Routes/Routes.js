
import React from 'react';

import Cronograma  from '../pages/cronograma';
import Appindex from '../pages/appindex';
import {BrowserRouter , Switch, Route} from 'react-router-dom'


export default function Routes() {

    return (
        <BrowserRouter>

    <Switch>        
            <Route path='/cronograma' component={Cronograma} />
            <Route  path='/' component={Appindex } />
            {/* <Route exact path='/login' component={ Login } /> */}
            
        </Switch>
        </BrowserRouter>

    )
}